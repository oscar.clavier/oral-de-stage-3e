---
author: Oscar Clavier--Dupérat
title: Oral de stage 3E
subtitle: "[https://s.42l.fr/oscar3E](https://s.42l.fr/oscar3E) <br/> [<i class='fa fa-file-pdf-o'></i> Le pdf](slides.pdf)"
date: 28 novembre 2023
---

# {data-background-image="../includes/oscar-full.jpg" data-state="white80"}

| | |
|-|-|
| Oscar Clavier--Dupérat | ![](../includes/oscar-zoom.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | oscar@tcweb.org |
|<i class="fa fa-instagram"></i>   | oscar.c_d |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    |  +33 7 68 25 92 97 |

</small>

:::notes
* classe etablissement
* âge
:::

# Mon projet professionel {data-background-image="../includes/projetPro.jpg" data-state="white70"}

* Bac scientifique
* Concours vétérinaire
* Vétérinaire de ville

# Pourquoi ce stage {data-background-image="../includes/carte.png" data-state="white80"}

* Recherche autour de chez moi (Roubaix, Croix et Zoo de Lille)
* Recherche avec l'aide de la famille
* Recherche étendue
* Choix

# Le lieu {data-background-image="../includes/bourges.jpg" data-state="white80"}

* 2h30 au sud de Paris
* Une agglomération de 86 000 habitants
* Lille - Bourges
* Marmagne - Bourges

# Clinique vétérinaire des Danjons {data-background-image="../includes/clinique.jpg" data-state="white80"}

* Vétérinaire d'animaux de compagnie
* 1 des 7 cabinets de l'agglomération
* L'aventure du docteur Guyot
* 1 cabinet pour 2 métiers

# Activités observés ou réalisées {data-background-image="../includes/suture.jpg" data-state="white80"}

* Castration d'un chat
* Détartrage des dents d'un chien
* Suture d'une gencive d'un chien

# Les métiers observés {data-background-image="../includes/batman.jpg" data-state="white80"}

* Vétérinaire
* Auxiliaire vétérinaire

# Les formations {data-background-image="../includes/env.jpg" data-state="white80"}

* Vétérinaire
  * 5 écoles en France
  * Un concours très difficile
  * Un diplôme Bac+6
* Auxiliaire vétérinaire
  * Une formation partout en France
  * Un diplôme Bac+2
  * De nombreuses spécialités

# Mes activités {data-background-image="../includes/vaccin.jpg" data-state="white80"}

* Entretien de la clinique
* Observation des consultations
* Assistance des opérations (vaccins, échographies, chirurgicales)

# Mes apprentissages {data-background-image="../includes/cerveau.jpg" data-state="white90"}

* Une super expérience
* De longues études : mais je vais y arriver !
* Un choix confirmé

# Merci {data-background-image="../includes/question.jpg" data-state="white80"}

Des questions ?

